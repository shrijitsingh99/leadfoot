# Project Manas AI Task Phase Final Project (Leadfoot)
No one likes going on a roadtrip with that one aggressive friend driving. So your job is to build an agent that can learn to use the throttle and brake as smoothly as an experienced driver.

# Requirements
This code is written in Python 3 and requires TensorFlow (r1.4 and later).

+ **Tensorflow:** [Installation](https://www.tensorflow.org/install/)

+ **Python 3:** [Installation](https://www.python.org/download/releases/3.0/)

# Usage 
All the data required to run this is in the `/data` directory.  
You can use your own data by replacing the `input_data.csv` and `output_data.csv` files.  
You can train the model and test in by running `tf.py`.  
