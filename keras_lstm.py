import numpy as np
import keras as keras
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.models import load_model

from keras.layers import Highway
from keras.layers.wrappers import Bidirectional
from keras.layers.noise import AlphaDropout
from keras.layers.normalization import BatchNormalization
from keras.layers.recurrent import LSTM, SimpleRNN
from keras.layers import TimeDistributed
from sklearn.preprocessing import MinMaxScaler

BATCH_SIZE = 2
TIMESTAMP = 10000

def read_data(input_file, output_file):
    input_data = np.genfromtxt(input_file, dtype=int, delimiter=',')
    output_data = np.genfromtxt(output_file, dtype=int, delimiter=',')
    return  input_data, output_data

def split_data_training_test(X, Y):
    X_training = X[0: 100000]
    X_test = X[100000: 120000]
    Y_training = (Y[0: 100000]) / 100.0
    Y_test = (Y[100000: 120000])/100.0


    print('\n', 'X Training: ', len(X_training), '\n', 'X Test: ',  len(X_test), '\n',  'Y Training: ',  len(Y_training), '\n', 'X Test: ',  len(Y_test), '\n')

    return (X_training.reshape(-1, BATCH_SIZE, TIMESTAMP, 1), Y_training.reshape(-1, BATCH_SIZE, TIMESTAMP, 2)), (X_test.reshape(-1, BATCH_SIZE, TIMESTAMP, 1), Y_test.reshape(-1, BATCH_SIZE, TIMESTAMP, 2))

if __name__ ==  "__main__":
    X, Y = read_data('data/input_data.csv', 'data/output_data.csv')
    (X_training, Y_training), (X_test, Y_test) = split_data_training_test(X, Y)
    print(X_training.shape, Y_training.shape)

    model = Sequential()
    model.add(LSTM(200, return_sequences=True, stateful=True, batch_input_shape=(BATCH_SIZE, TIMESTAMP, 1)))
    model.add(Dense(2))
    model.add(Activation("sigmoid"))
    model.compile(loss="mean_squared_error", optimizer="rmsprop")

    model.summary()
    loss = 0.0
    # load_model('rnn.h5')
    for run in range(0, 3):
        for i in range(X_training.shape[0]):
            print("Epoch: ", i)
            for j in range(8):
                model.train_on_batch(X_training[i], Y_training[i])
                print("Epoch: ", i, "Iteration: ", j)
            current_loss = model.evaluate(X_training[i], Y_training[i], batch_size=BATCH_SIZE)
            # model.reset_states()
            print(current_loss)
            loss += current_loss

    model.save("rnn-3.h5")

    print("Loss: ", loss/X_training.shape[0])

    prediction = []
    for i in range(X_test.shape[0]):
        prediction.append(model.predict(X_test[i]))
    predict = np.array(prediction).reshape(-1, 2)
    print(predict.shape)
    rmse = np.sqrt(((predict - Y_test.reshape(-1, 2)) ** 2).mean(axis=0))
    print(rmse)

    # Plot Prediction and Input Data
    plt.plot(predict[0:, 0], label="Throttle Prediction")
    plt.plot(Y_test.reshape(-1, 2)[0:, 0], label="Throttle Data")
    plt.plot(predict[0:, 1], label="Break Prediction")
    plt.plot(Y_test.reshape(-1, 2)[0:, 1], label="Break Data")
#    plt.plot(X_test_raw.reshape(-1, 1)/100.0, label="Velocity Data")
    plt.title("Predicted VS Input")
    plt.ylabel('Throttle/Break/Velocity')
    plt.legend()
    plt.show()





