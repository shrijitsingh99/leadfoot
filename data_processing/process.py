import numpy as np
import csv

with open('feedback.csv', 'r') as f:
  feedback_list = list(csv.reader(f))
with open('expert_drive.csv', 'r') as f:
    expert_drive_list = list(csv.reader(f))
with open('feedback_processed1.csv', 'r') as f:
  feedback_list_processed = list(csv.reader(f))

from numpy import genfromtxt
# feedback = genfromtxt('feedback.csv', delimiter=',')
# expert_drive = genfromtxt('expert_drive.csv', delimiter=',')

feedback = []
feedback_processed = []
expert_drive = []

for i in range(0, len(expert_drive_list)):
    expert_drive.append(list(map(int, expert_drive_list[i])))
for i in range(0, len(feedback_list)):
    feedback.append(list(map(int, feedback_list[i])))
for i in range(0, 140001):
    feedback_processed.append(list(map(int, feedback_list_processed[i])))

for i in range(0, 140001):
    if feedback_processed[i][2] == 0 and feedback_processed[i][0] == 0:
        # max = 0
        feedback_processed[i][2] = feedback_processed[i-1][2]
        # for j in range(i, 1400001):
        #     if feedback_processed[j] != 0:
        #         max = feedback_processed[j][0]
        # feedback_processed[i][2] = int((max + min/2))
for i in range(0, 140000):
    print(i)
    if feedback_processed[i][0] == 0:
        max = 0
        min = feedback_processed[i-1][0]
        k = 0
        for j in range(i, 140001):
            if feedback_processed[j][0] != 0:
                max = feedback_processed[j][0]
                break;
            k += 1
        feedback_processed[i][0] = min + (max - min)/k

np.savetxt('feedback_final.csv', np.array(feedback_processed), delimiter=',')
# np.savetxt('expert_drive_final.csv', np.array(expert_drive), delimiter=',')