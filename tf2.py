import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

#########################
#   Model parameters    #
#########################

TIMESTAMP = 10000
BATCH_SIZE = 2
LEARNING_RATE = 0.1
EPOCHS = 12


#######################
#   Data Processing   #
#######################

# Read data from file
def read_data(input_file, output_file):
    input_data = np.genfromtxt(output_file, dtype=int, delimiter=',')
    output_data = np.genfromtxt(input_file, dtype=int, delimiter=',')
    return  input_data, output_data

# Split data into training and testing
def split_data(X, Y):
    X_training = X[0: 100000]
    X_test = X[100000: 120000]
    Y_training = Y[0: 100000]
    Y_test = Y[100000: 120000]


    print('\n', 'X Training: ', len(X_training), '\n', 'X Test: ',  len(X_test), '\n',  'Y Training: ',  len(Y_training), '\n', 'X Test: ',  len(Y_test), '\n')

    return (X_training.reshape(-1, BATCH_SIZE, TIMESTAMP, 2), Y_training.reshape(-1, BATCH_SIZE, TIMESTAMP, 1)), (X_test.reshape(-1, BATCH_SIZE, TIMESTAMP, 2), Y_test.reshape(-1, BATCH_SIZE, TIMESTAMP, 1))


#############################
#   Cell State Functions    #
#############################

# Fetch cell state
def get_state_variables(batch_size, cell):
    # For each layer, get the initial state and make a variable out of it
    # to enable updating its value.
    state_variables = []
    for state_c, state_h in cell.zero_state(batch_size, tf.float32):
        state_variables.append(tf.contrib.rnn.LSTMStateTuple(
            tf.Variable(state_c, trainable=False),
            tf.Variable(state_h, trainable=False)))
    # Return as a tuple, so that it can be fed to dynamic_rnn as an initial state
    return tuple(state_variables)

# Update cell state
def get_state_update_op(state_variables, new_states):
    # Add an operation to update the train states with the last state tensors
    update_ops = []
    for state_variable, new_state in zip(state_variables, new_states):
        # Assign the new state to the state variables on this layer
        update_ops.extend([state_variable[0].assign(new_state[0]),
                           state_variable[1].assign(new_state[1])])
    # Return a tuple in order to combine all update_ops into a single operation.
    # The tuple's actual value should not be used.
    return tf.tuple(update_ops)

# Reset cell state to zeroes
def get_state_reset_op(state_variables, cell, batch_size):
    # Return an operation to set each variable in a list of LSTMStateTuples to zero
    zero_states = cell.zero_state(batch_size, tf.float32)
    return get_state_update_op(state_variables, zero_states)


#############
#   Model   #
#############

# Data Placeholder
input_data = tf.placeholder(dtype=tf.float32, shape=[BATCH_SIZE, TIMESTAMP, 2])
target_data = tf.placeholder(dtype=tf.float32, shape=[BATCH_SIZE, TIMESTAMP, 1])

# LSTM parameters
hidden_units =10
num_layers = 1

# LSTM layers
cell = tf.nn.rnn_cell.MultiRNNCell([tf.nn.rnn_cell.LSTMCell(hidden_units * i, state_is_tuple=True) for i in range(1, num_layers + 1)], state_is_tuple=True)

# Fetch cell state
states = get_state_variables(BATCH_SIZE, cell)

# Update cell state
lstm_output, new_states = tf.nn.dynamic_rnn(cell=cell, inputs=input_data, initial_state=states, dtype=tf.float32)

# Batch Normalization
norm = tf.contrib.layers.batch_norm(lstm_output, scale=True,  center=True)

# Fully connected output layer
predicted_outputs = tf.contrib.layers.fully_connected(norm, num_outputs=1, activation_fn=None)

# Update cell states with new cell states
update_op = get_state_update_op(states, new_states)

# Compute loss
mean_error = tf.losses.mean_squared_error(predicted_outputs, target_data)
optimizer = tf.train.AdamOptimizer(LEARNING_RATE).minimize(mean_error)

# Reset cell state to zeroes
reset_state_op = get_state_reset_op(states, cell, BATCH_SIZE)


#################
#   Training    #
#################

# Read data adn split it into training and testing
X, Y = read_data('data/input_data.csv', 'data/output_data.csv')
(X_training, Y_training), (X_test, Y_test) = split_data(X, Y)

session = tf.Session()
session.run(tf.global_variables_initializer())

# Reset cell state to zero before training
session.run([reset_state_op])

for epoch in range(EPOCHS):
    print("Epoch: ", epoch, '\n\n')
    # Error after each epoch
    epoch_error = 0
    for batch_index in range(X_training.shape[0]):
        # Error in each batch
        batch_error = 0
        print("Batch Index: ", batch_index)
        # Train model
        batch_error, optimize, prediction, cell_state = session.run([mean_error, optimizer, predicted_outputs, update_op], {
            input_data: X_training[batch_index],
            target_data: Y_training[batch_index],
        })
        epoch_error += batch_error
        print("Batch Train Error: %.5f" % batch_error)
        print("------------------------------------------")
    epoch_error /= X_training.shape[0]
    print("Batch Training Error: ", epoch_error)
    # Reset cell state after each epoch
    session.run([reset_state_op])
    print(".........................................................................")

print("########################################################################")
# Predicted output
predicted_result = []
# Testing data
testing_input = X_test
testing_target = Y_test
# Reset cell state to zeroes before testing
session.run([reset_state_op])
for batch_index in range(testing_input.shape[0]):
    batch_error, result, cell_state = session.run([mean_error, predicted_outputs, update_op], {
        input_data: testing_input[batch_index],
        target_data: testing_target[batch_index],
    })
    predicted_result.append(result)
    print("Batch %d, Batch Testing Error: %.4f" % (batch_index, batch_error))
print("########################################################################")

# Reshaped prediction output
prediction = np.array(predicted_result).reshape(-1, 1)

# Plot prediction and raw data
# Plot velocity
plt.plot(prediction[0:, 0], label="Velocity Prediction")
plt.plot(testing_target.reshape(-1, 1)[0:, 0], label="Velocity Data")
plt.title("Predicted VS Input")
plt.ylabel('Velocity')
plt.xlabel("Time")
plt.legend()
plt.show()

# # Save Model
# saver = tf.train.Saver()
# saver.save(session, './model.ckpt')

session.close()

